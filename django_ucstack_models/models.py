from django_mdat_customer.django_mdat_customer.models import *


class UcTenantSettings(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    full_access_eb = models.BooleanField(default=False)
    mailstore_enabled = models.BooleanField(default=False)
    msexchange_enabled = models.BooleanField(default=False)
    talk_enabled = models.BooleanField(default=False)
    meet_enabled = models.BooleanField(default=False)
    letsignit_enabled = models.BooleanField(default=False)

    @property
    def mailstore_id(self):
        try:
            value = int(self.customer.org_tag[0])
        except ValueError:
            return self.customer.org_tag.lower()

        return "i-" + self.customer.org_tag.lower()

    class Meta:
        db_table = "uc_tenant_settings"
