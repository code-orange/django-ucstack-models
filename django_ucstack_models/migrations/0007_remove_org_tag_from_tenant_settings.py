# Generated by Django 3.0.7 on 2020-06-20 16:07

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("django_ucstack_models", "0006_add_meet_enabled_flag"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="uctenantsettings",
            name="org_tag",
        ),
    ]
