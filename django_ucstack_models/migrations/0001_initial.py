# Generated by Django 3.0.7 on 2020-06-08 19:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("django_mdat_customer", "0008_add_enabled_field"),
    ]

    operations = [
        migrations.CreateModel(
            name="UcTenantSettings",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "org_tag",
                    models.CharField(
                        default=None, max_length=5, null=True, unique=True
                    ),
                ),
                (
                    "customer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.MdatCustomers",
                        unique=True,
                    ),
                ),
            ],
            options={
                "db_table": "uc_tenant_settings",
            },
        ),
    ]
