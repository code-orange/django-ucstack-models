# Generated by Django 3.0.7 on 2020-06-08 20:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_ucstack_models", "0005_add_talk_enabled_flag"),
    ]

    operations = [
        migrations.AddField(
            model_name="uctenantsettings",
            name="meet_enabled",
            field=models.BooleanField(default=False),
        ),
    ]
